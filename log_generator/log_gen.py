#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.handlers
import time
import json


log_fmt_str = '{"log_level": "%(levelname)s", "timestamp": "%(asctime)s.%(msecs)03dZ", "msg": %(message)s}'

class StructuredMessage(object):
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def __str__(self):
        return json.dumps(self.kwargs)


if __name__ == '__main__':
    # fix timezone to UTC
    logging.Formatter.converter = time.gmtime
    logging.basicConfig(level=logging.WARNING,
                        #filename = r'logfile.log',
                        format=log_fmt_str,
                        datefmt="%Y-%m-%dT%H:%M:%S",
                        filemode='w',
                        )
    logger = logging.getLogger(__name__)
    handler = logging.handlers.TimedRotatingFileHandler(filename='logfile.log', # log to test.log
                                                        when='D',               # rotate every day
                                                        backupCount=10,)        # keep 10 previous day backs


    # Create new handler/formatter for logging
    formatter = logging.Formatter(fmt=log_fmt_str)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    count = 0
    while True:
        count = count + 1
        log=StructuredMessage(msg_a=str(count) + ' th log started', count=str(count))
        logger.debug(log)
        log=StructuredMessage(msg1='this is debug', count=str(count))
        logger.debug(log)

        time.sleep(0.05)

        log=StructuredMessage(msg1='this is info', count=str(count))
        logger.info(log)

        time.sleep(0.05)

        log=StructuredMessage(msg1='this is warning', count=str(count))
        logger.warning(log)

        time.sleep(0.05)

        log=StructuredMessage(msg1='this is error', count=str(count))
        logger.error(log)

        time.sleep(0.05)

        log=StructuredMessage(message="this is critical message",
                                           number=count,
                                           TrueFalse=(count%2==0),
                                           array=[1,2,3,4,5,6],
                                           nested={"123":123, "456": "str_456"})
        logger.critical(log)

        time.sleep(0.05)

        log=StructuredMessage(msg_a=str(count) + ' th log stopped', count=str(count))
        logger.debug(log)

        time.sleep(1)
