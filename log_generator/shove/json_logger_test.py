import logging

from pythonjsonlogger import jsonlogger

fmt = "%(name)s %(levelname)s %(levelno)s %(pathname)s %(filename)s %(module)s %(lineno)d %(funcName)s \
%(created)f %(asctime)s %(msecs)d %(relativeCreated)d %(thread)d %(threadName)s %(process)d %(message)s"

logger = logging.getLogger(__name__)

logHandler = logging.StreamHandler()
formatter = jsonlogger.JsonFormatter(fmt=fmt)
logHandler.setFormatter(formatter)
logger.addHandler(logHandler)



logger.propagate = False

logger.warning("wtf", extra={"omg": 123})
