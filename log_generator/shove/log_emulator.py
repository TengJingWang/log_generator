#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import time

from log_generator import events

MACHINE_NUM = 10

verbose_fmt_str = "%(name)s %(levelname)s %(levelno)s %(pathname)s %(filename)s %(module)s %(lineno)d %(funcName)s \
%(created)f %(asctime)s %(msecs)d %(relativeCreated)d %(thread)d %(threadName)s %(process)d %(message)s"

short_fmt_str = "%(name)s %(levelname)s %(levelno)s %(pathname)s %(lineno)d %(funcName)s %(created)f\
 %(asctime)s %(msecs)d %(relativeCreated)d %(thread)d %(threadName)s %(process)d %(message)s"

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)


    game_events = []
    for x in range(0, MACHINE_NUM):
        game_events.append(events.CrashEvent("eventname", 0.05))

    while True:
        for event in game_events:
            event.update()

        time.sleep(1)



